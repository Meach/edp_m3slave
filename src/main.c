
#include <stdio.h>
#include "../lib/MCP4921.h"
#include "../lib/ads8353.h"
#include "../lib/core/spi.h"
#include "../lib/core/system.h"
#include "../lib/core/timer.h"
#include "../lib/core/uart.h"
#include "../lib/edp.h"
#include "../lib/MAX1321.h"
#define TOGGLE_PIN 28
/**  SystemTick_t   = (SYSTICK_SET - 1) / clk_cpu
 *                  = (121 - 1) / 120e6
 *                  = 1e-6
 *                  = 1us
 */
#define SYSTICK_SET 121
#define DAC1_CSPIN 6
#define ADC1_CSPIN 19

void GPIO_Toggle(void);
void NVIC_SetPriority(int IRQ_n,uint32_t priority);
void GPIO0_INTEN();

extern uint32_t __etext[];
extern uint32_t __data_start__[];
uint16_t adca_data_t[5800] = {0};
uint16_t adcb_data_t[5800] = {0};
MCP492x_t MCP4921_str;
ADC_str ADS8353_set= {0};
uint32_t TimeCounter = 0;
uint32_t DataCounter = 0;
volatile uint32_t interval = 0;
Pulse_Sequence EDP_pulse;
Time_Sequence  EDP_time;
uint16_t temp_adc1 = 0;
uint16_t temp_adc2 = 0;
uint16_t set_bit =0;
// uint16_t v_count = 0;
//test
int T_start = 1000;
int T_interval = 300;
int T_delay = 300;

int main (void) {
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    //System Init set
    system_clock_init();
    system_power_init();

    //USER Init set
    pinconnect_init();
    uart0_init();
    Timer0_Init();
    SSP0_Init();
    SSP1_Init();
    SysTick_Init(SYSTICK_SET);
    CS_Pinset(DAC1_CSPIN);
    CS_Pinset(ADC1_CSPIN);
// USER Code ADC Contral
    ADS8353_set.Input_Rang = 0;
    ADS8353_set.Data_Lines = 1;
    ADC_Config(&ADS8353_set);
//485 Init
    FIO0DIR |= (1 << 5);
    FIO0CLR |= (1 << 5);
//EDP Init
    // Init_PulseSquence(&EDP_pulse);
    // Init_TimeSquence(&EDP_time);
    // Add_Pulse(&EDP_pulse,0,0);
    // Add_Pulse(&EDP_pulse,820,1);
    // Add_Pulse(&EDP_pulse, 2457, 2);
    // Set_TimeSquence_Ts(&EDP_time, &EDP_pulse, 1000);
    // Set_TimeSquence_Tp(&EDP_time, &EDP_pulse, 300);
    // Set_TimeSquence_Td(&EDP_time, &EDP_pulse, 300);
    // USER Code DAC Contral
    MCP4921_str.gain = 1;
    MCP4921_str.chennal = 0;
    MCP4921_str.buffer_en = 0;
    MCP4921_str.shdn = 1;
    DAC_Ctor(&MCP4921_str,DAC1_CSPIN);
    MCP4921_str.Init_fp(&MCP4921_str);
    Delay(50);
    MCP4921_str.data = 2457;
    MCP4921_str.VoltageSet_fp(&MCP4921_str);
    NVIC_EnableIRQ(TIMER0_IRQn);         //Enable Timer Interrupt
    while (1) {
        // if (EDP_time.Sequence_done == 1) {
        //     NVIC_DisableIRQ(TIMER0_IRQn);
        //     for (uint16_t i = 0; i < EDP_time.T_Total; i++) {
        //         FIO0SET |= (1 << 5);
        //         printf("%x\n\r", adca_data_t[i]);
        //         FIO0CLR |= (1 << 5);
        //     }
        //     for (uint16_t i = 0; i < EDP_time.T_Total; i++) {
        //         adca_data_t[i] = 0;
        //     }
        //     DataCounter = 0;
        //     EDP_time.Sequence_done = 0;
        //     NVIC_EnableIRQ(TIMER0_IRQn);
        // }
        printf("Hell yeah ！！！\r\n");
    }

}

void GPIO_Toggle(void){
    // FIO4DIR |= (1 << TOGGLE_PIN);
    if (FIO4PIN & (1 << TOGGLE_PIN))
    {
        FIO4CLR |= (1<<TOGGLE_PIN);
    }else
    {
        FIO4SET |= (1<<TOGGLE_PIN);
    }
}

void SysTick_Handler(){
    if(TimeDelay !=0){
        TimeDelay--;
    }
}

void GPIO1_INTEN(){
    FIO2DIR &= ~(1<<11);
    PINSEL4 &= ~(3 << 22);
    PINSEL4 |= (1<<22);
    //EX EN
    EXTMODE = (1<<1);
    //FALLING EN
    // EXTPOLAR = (1<<1);
}


void TIMER0_IRQHandler(){
    ADC_Data(&temp_adc1,&temp_adc2, 1);
    adca_data_t[DataCounter] = temp_adc1;
    adcb_data_t[DataCounter] = temp_adc2;
    FIO0SET |= (1 << 5);
    printf("%x,%x\n\r", temp_adc1, temp_adc2);
    FIO0CLR |= (1 << 5);
    DataCounter++;
    // if (!(EDP_time.Ts_done)) {
    //     if (MCP4921_str.data!=2030) {
    //         MCP4921_str.data = 2030;
    //         MCP4921_str.VoltageSet_fp(&MCP4921_str);
    //         MCP4921_str.data = 2030;
    //         MCP4921_str.VoltageSet_fp(&MCP4921_str);
    //     }
    //     EDP_time.T_count++;
    // } else if (EDP_pulse.P_Count <= EDP_pulse.P_Total) {
    //     if (EDP_time.Tp_done == 1) {
    //         if (MCP4921_str.data != 2030) {
    //             MCP4921_str.data = 2030;
    //             MCP4921_str.VoltageSet_fp(&MCP4921_str);
    //         }
    //     } else{
    //         if (MCP4921_str.data != EDP_pulse.P_Squence[EDP_pulse.P_Count]) {
    //             MCP4921_str.data = EDP_pulse.P_Squence[EDP_pulse.P_Count];
    //             MCP4921_str.VoltageSet_fp(&MCP4921_str);
    //         }
    //     }
    //     EDP_time.T_count++;
    // }
    // if (EDP_time.T_count == EDP_time.Ts && EDP_time.Ts_done == 0) {
    //     EDP_time.Ts_done = 1;
    //     EDP_time.T_count = 0;
    // } else if (EDP_time.T_count == EDP_time.Tp && EDP_time.Td_done == 0 && EDP_time.Tp_done == 0 && EDP_time.Ts_done == 1) {
    //     EDP_time.Tp_done = 1;
    //     EDP_time.T_count = 0;
    //     EDP_pulse.P_Count++;

    // } else if (EDP_time.T_count == EDP_time.Td && EDP_time.Ts_done == 1) {
    //     EDP_time.Tp_done = 0;
    //     EDP_time.T_count = 0;

    // if (EDP_pulse.P_Count == EDP_pulse.P_Total) {
    //         EDP_pulse.P_Count = 0;
    //         EDP_time.Ts_done = 0;
    //         EDP_time.Sequence_done = 1;
    //         DataCounter = 0;
    //     }
    // }

    //Single Voltage Get
    if(DataCounter == 3000){
        EDP_time.Sequence_done = 1;
    }
    T0IR = 0x01;  // reset TIMER0 Interrupt
}

void EINT1_IRQHandler(void){
    EXTINT = (1 << 1);
}
