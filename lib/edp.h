#ifndef EDP_H
#define EDP_H
#include "./core/uart.h"
#include "inttypes.h"
#include "stdlib.h"
#define RESOLUTION_DAC  4095.0
#define PULSE_NUM   3
#define VOLTAGE_SEQUENCE_SIZE 10
#define VOLTAGE_ZERO 2048


// error
#define DONE 0
#define OVER_PULSE_SEQUENCE 1

typedef struct __EDP_Pulse_Sequence{
    uint16_t P_Squence[VOLTAGE_SEQUENCE_SIZE];
    uint16_t P_Total;
    uint16_t P_Count;
} Pulse_Sequence;
typedef struct __EDP_Time_Sequence {
    uint16_t Ts;
    uint16_t Td;
    uint16_t Tp;
    uint16_t Ts_done;
    uint16_t Td_done;
    uint16_t Tp_done;
    uint32_t T_count;
    uint32_t T_Total;
    uint16_t Sequence_done;
} Time_Sequence;

uint32_t Init_PulseSquence(Pulse_Sequence *Pulse_seq);
uint32_t Init_TimeSquence(Time_Sequence *Time_seq);
uint32_t Add_Pulse(Pulse_Sequence *Pulse_seq,uint16_t Pulse,uint16_t Sequence_num);
uint32_t Set_TimeSquence_Ts(Time_Sequence *Time_seq, Pulse_Sequence *Pulse_seq,
                            uint16_t ts);
uint32_t Set_TimeSquence_Tp(Time_Sequence *Time_seq, Pulse_Sequence *Pulse_seq,
                            uint16_t tp);
uint32_t Set_TimeSquence_Td(Time_Sequence *Time_seq, Pulse_Sequence *Pulse_seq,
                            uint16_t td);
#endif
