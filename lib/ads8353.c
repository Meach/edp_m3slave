#include "ads8353.h"

uint16_t ADC_Config(ADC_str *str){
    if (str == NULL)
    {
        return 88;
    }
    uint16_t CFR_Set[3]={0,0,0};
    CFR_Set[0] = (str->Data_Lines << 10) | (str->Input_Rang << 9) |
              (str->INM_Sel << 7) | (str->Ref_Sel << 6) | (str->SB << 5) |
              (str->Data_Format << 4) | (1 << 15);
    CS_Enable(19);
    SSP0_Transmit(CFR_Set, 3);
    CS_Disable(19);
    return 0;
}

uint16_t ADC_Data(uint16_t *data1,uint16_t *data2,uint16_t len){
    uint16_t* Buffer = (uint16_t*)malloc(3 * sizeof(len));
    for (int i = 0; i < len*3; i+=3)
    {
        CS_Enable(19);
        SSP0_Receive((uint16_t*)(Buffer + i), 3);
        CS_Disable(19);
    }
    for (uint16_t i = 0; i < (len*3); i+=3)
    {
        *(data1 + (i / 3)) = *(Buffer + i + 1);
        *(data2 + (i / 3)) = *(Buffer + i + 2);
    }
    free(Buffer);
    return 0;
}
