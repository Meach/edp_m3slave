#ifndef MAX1321_H
#define MAX1321_H

#include "LPC1769.h"
#include "regdef.h"
#include "./core/system.h"


//TEST BOARD
#define MAX_D0 0
#define MAX_D1 1
#define MAX_D2 2
#define MAX_D3 3
#define MAX_D4 4
#define MAX_D5 5
#define MAX_D6 6
#define MAX_D7 7
#define MAX_D8 8
#define MAX_D9 9
#define MAX_D10 10
#define MAX_D11 11
#define MAX_D12 12
#define MAX_D13 13
#define RD_BIT 28
// #define CS_BIT 14
// #define SHDN_BIT 0
// #define ALLON_BIT 1
#define EOC_BIT 25
#define EOLC_BIT 26
// #define WR_BIT 10
#define CONVST_BIT 29

//EDP BOARD
// #define RD_BIT 9
// #define CS_BIT 14
// #define SHDN_BIT 0
// #define ALLON_BIT 1
// #define EOC_BIT 4
// #define EOLC_BIT 8
// #define WR_BIT 10
// #define CONVST_BIT 15
// #define MAX_D0      16
// #define MAX_D1      17
// #define MAX_D2      18
// #define MAX_D3      19
// #define MAX_D4      20
// #define MAX_D5      21
// #define MAX_D6      22
// #define MAX_D7      23
// #define MAX_D8      24
// #define MAX_D9      25
// #define MAX_D10     26
// #define MAX_D11     27
// #define MAX_D12     28
// #define MAX_D13     29



void MAX1321_init();
void MAX1321_init_tb();

void MAX1321_get(uint32_t *CH0, uint32_t *CH1);
void RD_enable();
void RD_disable();
uint32_t ADC_read();
uint32_t EOC_trigger();
void CONVST_trigger();

#endif
