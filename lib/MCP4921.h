#ifndef MCP4921_H
#define MCP4921_H

#include "regdef.h"
#include "LPC1769.h"

#define PINSET_CS GPIO_PIN_3
#define PINSET_LDAC GPIO_PIN_0

typedef struct __dac_str{
    uint8_t CS;
    uint8_t chennal;
    uint8_t buffer_en;
    uint8_t gain;
    uint8_t shdn;
    uint16_t data;
    uint8_t (*Init_fp)(struct __dac_str *);
    uint8_t (*VoltageSet_fp)(struct __dac_str *);
    uint8_t (*ChennalSet_fp)(struct __dac_str *);
    uint8_t (*GainSet_fp)(struct __dac_str *);
} MCP492x_t;

// define HAL SPI struct
uint8_t DAC_Ctor(MCP492x_t *DacStr_p, uint8_t CSPIN);

uint8_t DAC_Init(MCP492x_t *DacStr_p);
uint8_t DAC_VolatageSet(MCP492x_t *DacStr_p);
uint8_t DAC_ChennalSet(MCP492x_t *DacStr_p);
uint8_t DAC_GainSet(MCP492x_t *DacStr_p);
#endif
