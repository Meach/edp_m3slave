/**
 * @file spi.c
 * @author Meach (you@domain.com)
 * @brief spi communication
 * @version 0.1
 * @date 2020-02-22
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "spi.h"
#include "stdlib.h"

/**
 * SPI
 *  clk_spi = clk_cpu / PCLKSEL_DEVIDER_x / (SPIDIV_VAL * [SCR + 1])
 *  example:
 *  clk_cpu = 120M
 *  PCLKSEL_DEVIDER_x = PCLKSEL_DEVIDER_1
 *  SPIDIV_VAL = 30
 *  SCR = 0
 *  clk_spi = 120e6 / 1 / (30 * [ 0 + 1 ])
 *          = 4e6
 *          =4M
 * note:SPIDIV_VAL can't be 0
 */

#define SPIDIV_VAL 30
#define SSP0DIV_VAL 30
#define SSP1DIV_VAL 100

#define SPI_BIT     8
#define SSP0_BIT    16
#define SSP1_BIT    16

void SPI_Init(){
    PCONP |= PCONP_SPI;
    PCLKSEL0 |= (PCLKSEL_DEVIDER_1 << 16);

    //SCK
    PINSEL0 |= (0x3 << 30);
    //SSEL
    PINSEL1 |= (0x3 << 0);
    //MOSI
    PINSEL1 |= (0x3 << 4);
    //MISO
    PINSEL1 |= (0x3 << 2);

    S0SPCCR = SPIDIV_VAL;

    S0SPCR |= SPI_S0SPCR_MSTR | SPI_S0SPCR_SPIE | SPI_S0SPCR_BITS_DSS(SPI_BIT);
}

void SSP1_Init(){
    PCONP |= PCONP_SSP1;
    PCLKSEL0 |= (PCLKSEL_DEVIDER_1<<20);
    //Clear
    PINSEL0 &= ~((0x3 << 12) | (0x3 << 14) | (0x3 << 16) | (0x3 << 18));
    //SSEL1
    PINSEL0 |= (0x2 << 12);
    //SCK1
    PINSEL0 |= (0x2 << 14);
    //MOSI1
    PINSEL0 |= (0x2 << 16);
    //MISO1
    PINSEL0 |= (0x2 << 18);
    // SCR = 0;
    SSP1CPSR = SSP1DIV_VAL;
    SSP1CR0 = SSP_CR0_DSS(SSP1_BIT);

    SSP1CR1 = SSP_CR1_SSE;   //SSP1 Enable
}

void SSP0_Init() {
    PCONP |= PCONP_SSP0;
    PCLKSEL1 |= (PCLKSEL_DEVIDER_1 << 10);
    // Clear
    PINSEL0 &= ~(0x03 << 30);
    PINSEL1 &= ~((0x3 << 0) | (0x3 << 2) | (0x3 << 4));
    // SSEL0
    PINSEL1 |= (0x2 << 0);
    // SCK0
    PINSEL0 |= (0x2 << 30);
    // MOSI0
    PINSEL1 |= (0x2 << 4);
    // MISO0
    PINSEL1 |= (0x2 << 2);

    SSP0CPSR = SSP0DIV_VAL;
    SSP0CR0 = SSP_CR0_DSS(SSP0_BIT) | (1<<7);

    SSP0CR1 = SSP_CR1_SSE;  // SSP0 Enable
}

void SPI_Transmit(uint16_t *tx, uint16_t len){
    while (len--)
    {
        if (tx == NULL)
        {
            S0SPDR = 0x00;
        }else
        {
            S0SPDR = *tx++;
            while (!(S0SPSR & SPI_S0SPSR_SPIF))
                ;
        }
    }
}

void SPI_Receive(uint16_t *rx, uint16_t len) {
    while (len--) {
        S0SPDR = 0x00;
        while (!(S0SPSR & SPI_S0SPSR_SPIF))
            ;
        *rx++ = S0SPDR;
    }
}

void SPI_TxRx(uint16_t *tx,uint16_t *rx,uint16_t len){
    __attribute__((unused)) uint8_t dummy = 0;
    while (len--)
    {
        if (tx == NULL)
        {
            S0SPDR = 0x00;
        }else
        {
            S0SPDR = *tx++;
        }
        while (!(S0SPSR & SPI_S0SPSR_SPIF))
            ;
        if (rx == NULL)
        {
            dummy = S0SPDR;
        }else
        {
            *rx++ = S0SPDR;
        }
    }
}

void SSP1_Transmit(uint16_t *tx, uint16_t len){
    while (len--)
    {
        if(tx == NULL){
            SSP1DR = 0x00;
        } else {
            while ((SSP1SR & (SSP_SR_BSY | SSP_SR_TNF)) != (SSP_SR_TNF))
                ;
            SSP1DR = *tx++;
            while (SSP1SR & SSP_SR_BSY)
                ;
        }
    }
}

void SSP1_Receive(uint16_t *rx, uint16_t len){
    while (len--) {
        SSP1DR = 0x00;
        while ((SSP1SR & (SSP_SR_BSY | SSP_SR_RNE)) != (SSP_SR_RNE))
            ;
        *rx++ = SSP1DR;
    }
}

void SSP0_Transmit(uint16_t *tx, uint16_t len) {
    while (len--) {
        if (tx == NULL) {
            SSP0DR = 0x00;
        } else {
            while ((SSP0SR & (SSP_SR_BSY | SSP_SR_TNF)) != (SSP_SR_TNF))
                ;
            SSP0DR = *tx++;
            while (SSP0SR & SSP_SR_BSY)
                ;
        }
    }
}

void SSP0_Receive(uint16_t *rx, uint16_t len) {
    while (len--) {
        SSP0DR = 0x00;
        while ((SSP0SR & (SSP_SR_BSY | SSP_SR_RNE)) != (SSP_SR_RNE))
            ;
        *rx++ = SSP0DR;
        // while (SSP0SR & SSP_SR_BSY)
        //     ;
    }
}

void CS_Pinset(uint8_t CSPIN) {
    FIO0DIR |= (1 << CSPIN);
    FIO0SET |= (1 << CSPIN);
}

void CS_Enable(uint8_t CSPIN){
    FIO0CLR |= (1 << CSPIN);
}

void CS_Disable(uint8_t CSPIN){
    FIO0SET |= (1 << CSPIN);
}
