#ifndef SPI_H
#define SPI_H


#include "LPC1769.h"
#include "regdef.h"
/**
 * SPI      SCK     P0.15
 *          SSEL    P0.16
 *          MIS0    P0.17
 *          MOSI    P0.18
 * SSP0     SCK0    P0.15
 *          SSEL0   P0.16
 *          MISO0   P0.17
 *          MOSI0   P0.18
 * SSP1     SCK1    P0.7
 *          SSEL1   P0.6
 *          MISO1   P0.8
 *          MOSI1   P0.9
 */

void SPI_Init();
void SPI_Transmit(uint16_t *tx, uint16_t len);
void SPI_Receive(uint16_t *rx, uint16_t len);
void CS_Pinset(uint8_t CSPIN);
void CS_Enable(uint8_t CSPIN);
void CS_Disable(uint8_t CSPIN);
void SPI_TxRx(uint16_t *tx, uint16_t *rx, uint16_t len);

void SSP1_Init();
void SSP1_Transmit(uint16_t *tx, uint16_t len);
void SSP1_Receive(uint16_t *rx, uint16_t len);

void SSP0_Init();
void SSP0_Transmit(uint16_t *tx, uint16_t len);
void SSP0_Receive(uint16_t *rx, uint16_t len);
#endif
