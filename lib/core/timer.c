#include "timer.h"

#define CLK_M   119

// max = 0.54us ~ 1.8MHz
/** TIME_s  = Pclk / PCLKSEL0 / CLK_M
 *          = 120MHz / 1 / 119
 *          = 1MHz ~= 1us
 */

void Timer0_Init(){
    // Timer0 power on
    PCONP |= PCONP_TIM0;
    // Perioheral clock divider
    PCLKSEL0 |=(PCLKSEL_DEVIDER_1 << 2);
    T0TCR = 0x02;
    T0MR0 = 119;
    T0MCR |= (1<<0) | (1 << 1);
    T0IR = 0xff;

    //Timer0 enable
    T0TCR = 0x01;
}



void Timer0_isr(void){
}
