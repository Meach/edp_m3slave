#ifndef ADS8353_H
#define ADS8353_H

#include "./core/spi.h"
#include "stdlib.h"
#include "LPC1769.h"

typedef struct __adc_str
{
    uint16_t num;
    uint16_t Data_Lines;
    uint16_t Input_Rang;
    uint16_t INM_Sel;
    uint16_t Ref_Sel;
    uint16_t SB;
    uint16_t Data_Format;
}ADC_str;

typedef struct __adc_type {
    ADC_str type;

    uint16_t (*__ADC_Conf)(void);
}ADC_Type;

ADC_Type* ADC_Ctor(uint16_t num);
uint16_t ADC_Data(uint16_t* data1, uint16_t* data2, uint16_t len);
uint16_t ADC_Config(ADC_str* str);

#endif
