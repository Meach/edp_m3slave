#include "edp.h"

uint32_t Add_Pulse(Pulse_Sequence *Pulse_seq,uint16_t Pulse,uint16_t Sequence_num){
    if (Sequence_num > Pulse_seq->P_Total) {
        return OVER_PULSE_SEQUENCE;
    }
    Pulse_seq->P_Squence[Sequence_num] = Pulse;
    Pulse_seq->P_Total++;
    return DONE;
}

uint32_t Init_PulseSquence(Pulse_Sequence *Pulse_seq){
    for (int i = 0; i < VOLTAGE_SEQUENCE_SIZE; i++) {
        Pulse_seq->P_Squence[i] = 0;
    }
    Pulse_seq->P_Total = 0;
    Pulse_seq->P_Count = 0;

    return 0;
}

uint32_t Init_TimeSquence(Time_Sequence *Time_seq){
    Time_seq->Td = 0;
    Time_seq->Tp = 0;
    Time_seq->Ts = 0;
    Time_seq->Td_done = 0;
    Time_seq->Tp_done = 0;
    Time_seq->Ts_done = 0;
    Time_seq->T_count = 0;
    Time_seq->Sequence_done = 0;

    return 0;
}

uint32_t Set_TimeSquence_Ts(Time_Sequence *Time_seq, __attribute__((unused)) Pulse_Sequence *Pulse_seq,
                            uint16_t ts) {
    if (Time_seq->Ts > 0)
    {
        Time_seq->T_Total -= Time_seq->Ts;
    }
    Time_seq->Ts = ts;
    Time_seq->T_Total += Time_seq->Ts;
    // printf("EDP_TIME_TS = %d\n\r", Time_seq->T_Total);

    return 0;
}

uint32_t Set_TimeSquence_Tp(Time_Sequence *Time_seq, Pulse_Sequence *Pulse_seq,
                            uint16_t tp) {
    if (Time_seq->Tp > 0) {
        Time_seq->T_Total -= (Time_seq->Tp * Pulse_seq->P_Total);
    }
    Time_seq->Tp = tp;
    Time_seq->T_Total += (Time_seq->Tp * Pulse_seq->P_Total);
    // printf("EDP_TIME_TP = %d\n\r", Time_seq->T_Total);

    return 0;
}

uint32_t Set_TimeSquence_Td(Time_Sequence *Time_seq, Pulse_Sequence *Pulse_seq,
                            uint16_t td) {
    if (Time_seq->Td > 0) {
        Time_seq->T_Total -= (Time_seq->Td * (Pulse_seq->P_Total-1));
    }
    Time_seq->Td = td;
    Time_seq->T_Total += (Time_seq->Td * (Pulse_seq->P_Total-1));
    // printf("EDP_TIME_TD = %d\n\r", Time_seq->T_Total);

    return 0;
}
