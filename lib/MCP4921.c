#include "MCP4921.h"
#include "./core/spi.h"
#include"stdlib.h"
uint8_t DAC_Ctor(MCP492x_t *DacStr_p, uint8_t CSPIN) {
    if (DacStr_p != NULL)
    {
        DacStr_p->CS = CSPIN;
        DacStr_p->Init_fp = DAC_Init;
        DacStr_p->VoltageSet_fp = DAC_VolatageSet;
        DacStr_p->ChennalSet_fp = DAC_ChennalSet;
        DacStr_p->GainSet_fp = DAC_GainSet;
        return 0;
    }else
    {
        return 88;
    }
}

uint8_t DAC_Init(MCP492x_t *DacStr_p){
    if (DacStr_p == NULL)
    {
        return 88;
    }
    uint16_t data;
    data = ((DacStr_p->chennal) << 7) | ((DacStr_p->buffer_en) << 6) |
             ((DacStr_p->gain) << 5) | ((DacStr_p->shdn) << 4);
    CS_Pinset(DacStr_p->CS);
    CS_Enable(DacStr_p->CS);
    SSP1_Transmit(&data,1);
    CS_Disable(DacStr_p->CS);
    return 0;
}

uint8_t DAC_VolatageSet(MCP492x_t *DacStr_p){
    if(DacStr_p == NULL){
        return 88;
    }
    uint8_t data_H = 0;
    uint8_t data_L = 0;
    uint16_t data = 0;
    data_H = ((DacStr_p->chennal) << 7) | ((DacStr_p->buffer_en) << 6) |
             ((DacStr_p->gain) << 5) | ((DacStr_p->shdn) << 4);
    if (DacStr_p->data < 4096) {
        data_H |= (((DacStr_p->data) & 0xf00) >> 8);
        data_L = ((DacStr_p->data) & 0Xff);
        data = (data_H<<8) | (data_L);
        CS_Enable(DacStr_p->CS);
        SSP1_Transmit(&data, 1);
        CS_Disable(DacStr_p->CS);
        return 0;
    } else {
        return 1;
    }
}

uint8_t DAC_ChennalSet(MCP492x_t *DacStr_p){
    if (DacStr_p == NULL) {
        return 88;
    }
    uint16_t data;
    data = ((DacStr_p->chennal) << 7);

    CS_Enable(DacStr_p->CS);
    SSP1_Transmit(&data,1);
    CS_Disable(DacStr_p->CS);
    return 0;
}

uint8_t DAC_GainSet(MCP492x_t *DacStr_p){
    if (DacStr_p == NULL) {
        return 88;
    }
    uint16_t data;
    data = ((DacStr_p->gain) << 5);

    CS_Enable(DacStr_p->CS);
    SSP1_Transmit(&data,1);
    CS_Disable(DacStr_p->CS);
    return 0;
}
