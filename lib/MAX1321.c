#include "MAX1321.h"

void MAX1321_init(){
    FIO0DIR &= ~((1<<EOC_BIT) | (1<<EOLC_BIT));
    FIO0DIR |= (1 << CONVST_BIT) | (1<<RD_BIT);
    FIO0SET |= (1<<CONVST_BIT) | (1<<RD_BIT);
}

void MAX1321_init_tb(){
    // P1 EOC, ELOC
    FIO0DIR  &= ~((1 << EOC_BIT) | (1 << EOLC_BIT));
    FIO0MASK |= ~((1 << EOC_BIT) | (1 << EOLC_BIT));
    // P4 -> CONVST, RD
    FIO4DIR |= (1 << CONVST_BIT) | (1 << RD_BIT);
    FIO4SET |= (1 << CONVST_BIT) | (1 << RD_BIT);
    // P2 DATA
    FIO2DIR &= ~((1 << MAX_D0) | (1 << MAX_D1) | (1 << MAX_D2) | (1 << MAX_D3) |
                 (1 << MAX_D4) | (1 << MAX_D5) | (1 << MAX_D6) | (1 << MAX_D7) |
                 (1 << MAX_D8) | (1 << MAX_D9) | (1 << MAX_D10) |
                 (1 << MAX_D11) | (1 << MAX_D12) | (1 << MAX_D13));

    FIO2MASK |= ~((1 << MAX_D0) | (1 << MAX_D1) | (1 << MAX_D2) | (1 << MAX_D3) |
                 (1 << MAX_D4) | (1 << MAX_D5) | (1 << MAX_D6) | (1 << MAX_D7) |
                 (1 << MAX_D8) | (1 << MAX_D9));
}


void MAX1321_get(uint32_t *CH0, uint32_t *CH1){
    uint8_t ch_num = 0;
    uint32_t temp[2] = {0};
    CONVST_trigger();
    while (ch_num<2)
    {
        if (EOC_trigger()) {
            // RD_enable();
            FIO4CLR |= (1 << RD_BIT);
            temp[ch_num] = ADC_read();
            RD_disable();
            ch_num++;
        }

    }
    *CH0 = temp[0];
    *CH1 = temp[1];
}


void RD_enable(){
    FIO4CLR |=(1<<RD_BIT);
}

void RD_disable(){
    FIO4SET |= (1 <<RD_BIT);
}

uint32_t ADC_read(){
    return FIO2PIN;
}

uint32_t EOC_trigger(){
    return (FIO0PIN & (1 << EOC_BIT));
}

void CONVST_trigger(){
    FIO4CLR |= (1 << CONVST_BIT);
    FIO4SET |= (1 << CONVST_BIT);
}
